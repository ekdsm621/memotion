package com.alpa.memotion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MemotionApplication {

	public static void main(String[] args) {
		SpringApplication.run(MemotionApplication.class, args);
	}

}
